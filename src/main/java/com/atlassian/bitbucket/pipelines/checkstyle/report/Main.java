/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report;

import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestAnnotation;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.ImmutableRestAnnotation;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestReport;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.ImmutableRestReport;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.ImmutableRestReportData;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestReportData;
import io.reactivex.Observable;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.util.EntityUtils;
import se.bjurr.violations.lib.ViolationsApi;
import se.bjurr.violations.lib.model.SEVERITY;
import se.bjurr.violations.lib.model.Violation;
import se.bjurr.violations.lib.reports.Parser;

/**
 *
 * @author mkleint
 */
public class Main {

    private static final String ENV_COMMIT = "BITBUCKET_COMMIT";
    private static final String ENV_CLONE_DIR = "BITBUCKET_CLONE_DIR";
    private static final String ENV_REPO_FULL_NAME = "BITBUCKET_REPO_FULL_NAME";
    private static final String ENV_CHECKSTYLE_PATTERN = "CHECKSTYLE_RESULT_PATTERN";
    private static final String ENV_CHECKSTYLE_REPORT_ID = "CHECKSTYLE_REPORT_ID";
    private static final String ENV_FAIL_ON_SEVERITY = "REPORT_FAIL_SEVERITY";
    private static final String ENV_DEBUG = "DEBUG";
    private static final int ANNOTATIONS_BATCH_SIZE = 50;
    private static final long ANNOTATIONS_MAX_SIZE = 1000;

    public static void main(String[] args) {
        final String commit = System.getenv(ENV_COMMIT);
        final String rootDir = System.getenv(ENV_CLONE_DIR);
        final Path rootDirPath = Paths.get(rootDir);
        final String repoName = System.getenv(ENV_REPO_FULL_NAME);
        String external = System.getenv(ENV_CHECKSTYLE_REPORT_ID);
        if (external == null || external.trim().isEmpty()) {
            external = "checkstyle-1";
        }
        final String reportExternalId = external;
        String pattern = System.getenv(ENV_CHECKSTYLE_PATTERN);
        if (pattern == null || pattern.trim().isEmpty()) {
            pattern = ".*/checkstyle-result.xml$";
        }
        final String filePattern = pattern;
        
        final SEVERITY failLevel = envToSeverity(System.getenv(ENV_FAIL_ON_SEVERITY));
        final boolean debug = Boolean.parseBoolean(System.getenv(ENV_DEBUG) != null ?
                System.getenv(ENV_DEBUG) : "false");

        List<Violation> violations = ViolationsApi.violationsApi()
                .withPattern(filePattern)
                .inFolder(rootDir)
                .findAll(Parser.CHECKSTYLE)
                .violations();
        
        violations.sort(new SeverityComparator());

        long infoViolations = violations.stream()
                .filter((Violation t) -> SEVERITY.INFO.equals(t.getSeverity()))
                .count();
        long warnViolations = violations.stream()
                .filter((Violation t) -> SEVERITY.WARN.equals(t.getSeverity()))
                .count();
        long errorViolations = violations.stream()
                .filter((Violation t) -> SEVERITY.ERROR.equals(t.getSeverity()))
                .count();


        ImmutableRestReport.Builder report = ImmutableRestReport.builder()
                .reportType(RestReport.ReportType.BUG)
                .externalId(reportExternalId)
                .title("Checkstyle")
                .details("Checkstyle report")
                .result(reportResult(infoViolations, warnViolations, errorViolations, failLevel));
        report.data(io.vavr.collection.List.of(
                ImmutableRestReportData.builder()
                        .type(RestReportData.Type.NUMBER)
                        .title("Violations")
                        .value(violations.size())
                        .build(),
                ImmutableRestReportData.builder()
                        .type(RestReportData.Type.NUMBER)
                        .title("Low Severity")
                        .value(infoViolations)
                        .build(),
                ImmutableRestReportData.builder()
                        .type(RestReportData.Type.NUMBER)
                        .title("Medium Severity")
                        .value(warnViolations)
                        .build(),
                ImmutableRestReportData.builder()
                        .type(RestReportData.Type.NUMBER)
                        .title("High Severity")
                        .value(errorViolations)
                        .build()
        ));

        HttpHost proxy = new HttpHost("host.docker.internal", 29418);
        DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
        try {
            HttpPut reportPut = new HttpPut("http://api.bitbucket.org/2.0/repositories/" + repoName + "/commit/" + commit + "/reports/" + reportExternalId);
            reportPut.setEntity(new StringEntity(JSONUtil.writeValueAsString(report.build()), ContentType.APPLICATION_JSON));
            try (CloseableHttpClient httpclient = createClient(routePlanner)) {
                CloseableHttpResponse response = httpclient.execute(new HttpHost("api.bitbucket.org"), reportPut);
                System.out.println("Submitted report. Response:" + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
                if (debug) {
                    debugRequestResponse(reportPut, JSONUtil.writeValueAsString(report.build()), response);
                }
            }

            List<RestAnnotation> restViolations = violations.stream()
                    .map((Violation t) -> {
                        Path p = Paths.get(t.getFile());
                        return ImmutableRestAnnotation.builder()
                                .summary(t.getMessage())
                                .details(t.getRule())
                                .annotationType(RestAnnotation.Type.CODE_SMELL)
                                .externalId(createExternalId(t, reportExternalId))
                                .severity(toSeverity(t.getSeverity()))
                                .path(rootDirPath.relativize(p).toString())
                                .line(t.getStartLine().longValue())
                                .build();
                    }).collect(Collectors.toList());
            if (!restViolations.isEmpty()) {
                if (restViolations.size() > ANNOTATIONS_MAX_SIZE) {
                    System.out.println("We have too many annotations, only the first " + ANNOTATIONS_MAX_SIZE + " will be uploaded.");
                }
                Observable.fromIterable(restViolations).take(ANNOTATIONS_MAX_SIZE).buffer(ANNOTATIONS_BATCH_SIZE).forEach((List<RestAnnotation> batch) -> {
                    try (CloseableHttpClient httpclient = createClient(routePlanner)) {
                        HttpPost reportPost = new HttpPost("http://api.bitbucket.org/2.0/repositories/" + repoName + "/commit/" + commit + "/reports/" + reportExternalId + "/annotations");
                        reportPost.setEntity(new StringEntity(JSONUtil.writeValueAsString(batch), ContentType.APPLICATION_JSON));
                        CloseableHttpResponse response2 = httpclient.execute(new HttpHost("api.bitbucket.org"), reportPost);
                        System.out.println("Submitted " +  batch.size() + " annotations. Response:" + response2.getStatusLine().getStatusCode() + " " + response2.getStatusLine().getReasonPhrase());
                        if (debug) {
                            debugRequestResponse(reportPost, JSONUtil.writeValueAsString(batch), response2);
                        }
                    } catch (Throwable t) {
                        System.out.println("Catched exception" + t);
                    } finally {
                    }
                });
            }

        } catch (Exception ex) {
            System.out.println("Exception thrown:" + ex);
            ex.printStackTrace();
            System.exit(1);
        }
    }

    private static void debugRequestResponse(HttpEntityEnclosingRequestBase request, String requestBody, CloseableHttpResponse response) throws IOException, ParseException {
        System.out.println("Request URI:" + request.getURI());
        System.out.println("Request Body -------");
        System.out.println(requestBody);
        System.out.println("--------------------");
        System.out.println("Response Body ------");
        System.out.println(EntityUtils.toString(response.getEntity(), "UTF-8"));
        System.out.println("--------------------");
    }

    private static RestReport.Result reportResult(long infos, long warns, long errors,  SEVERITY failLevel) {
        long sum = 0;
        if (SEVERITY.INFO.equals(failLevel)) {
            sum = infos + warns + errors;
        }
        if (SEVERITY.WARN.equals(failLevel)) {
            sum = warns + errors;
        }
        if (SEVERITY.ERROR.equals(failLevel)) {
            sum =  errors;
        }
        return sum == 0 ? RestReport.Result.PASSED : RestReport.Result.FAILED;
    }

    private static CloseableHttpClient createClient(DefaultProxyRoutePlanner routePlanner) {
        CloseableHttpClient httpclient = HttpClients.custom().setRoutePlanner(routePlanner).build();
        return httpclient;
    }

    private static RestAnnotation.Severity toSeverity(SEVERITY severity) {
        switch (severity) {
            case ERROR:
                return RestAnnotation.Severity.HIGH;
            case WARN:
                return RestAnnotation.Severity.MEDIUM;
            case INFO:
                return RestAnnotation.Severity.LOW;
            default:
                return RestAnnotation.Severity.UNKNOWN;
        }
    }

    private static String createExternalId(Violation t, String reportExternalId) {
        return DigestUtils.sha1Hex(reportExternalId + ":" + t.getFile() + ":" + t.getStartLine() + ":" + t.getRule() + ":" + t.getMessage());
    }

    private static SEVERITY envToSeverity(String envValue) {
        if (envValue == null) {
            return SEVERITY.INFO; //default value
        }
        envValue = envValue.toUpperCase(Locale.ENGLISH).trim();
        try {
            return SEVERITY.valueOf(envValue);
        } catch (IllegalArgumentException ex) {
            return SEVERITY.INFO;
        }
    }
}
