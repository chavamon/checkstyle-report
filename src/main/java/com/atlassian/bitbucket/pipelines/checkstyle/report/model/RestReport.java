/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.vavr.collection.List;
import org.immutables.value.Value;



/**
 * The report on a code analysis conducted by an external provider.
 */
@Value.Immutable
@JsonDeserialize(builder = ImmutableRestReport.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface RestReport {

    enum ReportType {
        SECURITY,
        COVERAGE,
        TEST,
        BUG,
        UNKNOWN
    }

    enum Result {
        PASSED,
        FAILED,
        PENDING,
        UNKNOWN
    }

    String getTitle();

    String getDetails();

    @JsonProperty("external_id")
    String getExternalId();

    @JsonProperty("report_type")
    ReportType getReportType();

    Result getResult();

    List<RestReportData> getData();

}
