/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report;

import com.atlassian.bitbucket.pipelines.checkstyle.report.model.ImmutableRestAnnotation;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.ImmutableRestReport;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.ImmutableRestReportData;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestAnnotation;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestReport;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestReportData;
import io.vavr.collection.List;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author mkleint
 */
public class JSONUtilTest {

    public JSONUtilTest() {
    }


    @org.junit.jupiter.api.Test
    public void testWriteReportValueAsString() {
        Object value = ImmutableRestReport.builder()
                .title("title")
                .details("details")
                .result(RestReport.Result.PASSED)
                .externalId("id")
                .reportType(RestReport.ReportType.SECURITY)
                .data(List.of(ImmutableRestReportData.builder()
                        .title("title")
                        .type(RestReportData.Type.TEXT)
                        .value("value")
                        .build()))
                .build();
        String expResult = "{\"title\":\"title\",\"details\":\"details\",\"external_id\":\"id\",\"report_type\":\"SECURITY\",\"result\":\"PASSED\",\"data\":[{\"title\":\"title\",\"type\":\"TEXT\",\"value\":\"value\"}]}";
        String result = JSONUtil.writeValueAsString(value);
        assertEquals(expResult, result);
    }

       @org.junit.jupiter.api.Test
    public void testWriteAnnotationValueAsString() {
        Object value = ImmutableRestAnnotation.builder()
                .annotationType(RestAnnotation.Type.VULNERABILITY)
                .details("details")
                .externalId("id")
                .line(1L)
                .path("path")
                .severity(RestAnnotation.Severity.MEDIUM)
                .summary("summary")
                .build();
        String expResult = "{\"external_id\":\"id\",\"annotation_type\":\"VULNERABILITY\",\"path\":\"path\",\"line\":1,\"summary\":\"summary\",\"details\":\"details\",\"severity\":\"MEDIUM\"}";
        String result = JSONUtil.writeValueAsString(value);
        assertEquals(expResult, result);
    }

}
