# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: Fix default values for pipe variables

## 0.1.6

- patch: bugfix

## 0.1.5

- patch: add debug env variable DEBUG that lists the request and responses from bitbucket.org

## 0.1.4

- patch: Fix example descriptions in the Readme.

## 0.1.3

- patch: correct quoting in pipe.yml

## 0.1.2

- patch: changes to pipe.yml

## 0.1.1

- patch: update pipe variables

## 0.1.0

- minor: Add configuration to only fail Code Insight report when certain severity of violations occur
- minor: Sort annotations by severity to submit the more severe ones first.

## 0.0.2

- patch: Documentation and build updates

## 0.0.1

- patch: Initial version

